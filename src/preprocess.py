from typing import Dict, List, Tuple
import math


def _pad(string: str, n: int) -> str:
    if len(string) < n:
        m = n - len(string)
        return f"{string}{'0'*m}"
    return string


def preprocess_line(line: str, n: int) -> List[str]:
    if len(line) <= n:
        return [_pad(line.strip("\n"), n)]
    step = math.floor(n / 2)
    i_count = math.floor(len(line) / (n / 2))
    lines = [
        line[i * step : min(i * step + n, len(line) - 1)].strip("\n")
        for i in range(0, i_count)
    ]
    return [_pad(line, n) for line in lines]


def preprocess_file(data_path: str, n: int) -> Tuple[str, str]:
    data_name = f"{data_path}.preprocessed"
    chunk_map = f"{data_path}.chunkmap"

    with open(data_path, "r") as f_in:
        with open(data_name, "w") as f_out:
            with open(chunk_map, "w") as f_chunk:
                chunk_start = 0
                chunk_end = 0
                lines = f_in.readlines()
                for line_number, line in enumerate(lines):
                    chunks = preprocess_line(line, n)
                    if line_number == len(lines) - 1:
                        chunks = [
                            c + "\n" if i < len(chunks) - 1 else c
                            for i, c in enumerate(chunks)
                        ]
                    else:
                        chunks = [c + "\n" for c in chunks]
                    f_out.writelines(chunks)
                    chunk_end += len(chunks)
                    f_chunk.write(f"{line_number}, {chunk_start}, {chunk_end}\n")
                    chunk_start = chunk_end

    return data_name, chunk_map


def preprocess_syscalls(syscall_dict: Dict, n: int) -> Dict:
    preprocessed = {}
    for type_name, type_dict in syscall_dict.items():
        train_file, train_chunkmap = preprocess_file(type_dict.get("train"), n)
        preprocessed[type_name] = {
            "alpha": type_dict.get("alpha"),
            "train": train_file,
            "train_chunkmap": train_chunkmap,
            "tests": [],
        }
        for test_file in type_dict.get("tests"):
            label_file = f"{test_file.split('.test')[0]}.labels"
            t_f, t_chunkmap = preprocess_file(test_file, n)
            preprocessed[type_name]["tests"].append(
                {"data": t_f, "labels": label_file, "chunkmap": t_chunkmap}
            )
    return preprocessed
