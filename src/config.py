"""Configuration of NegSel2 and pipelines for each exercise.
Values in this file are most likely not needed to be changed:
values that depend on the environment are supplied by .env file.
"""

from dotenv import load_dotenv
from typing import List
import os

load_dotenv()

# root directory of the project
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


def full_path(base_dir: str, *paths: str):
    """Given a base directory and a list of relative paths, get the absolute path."""
    return os.path.join(base_dir, *paths)


# path to the NegSel2's jar
JAR_PATH = full_path(BASE_DIR, os.getenv("JAR_PATH"))

# A set of NegSel2's CLI arguments. Further documentation can be seen in negative_selection.py's
# TextorNegativeSelection docstrings.
NEGSEL2_ARGS = {
    "command": {"type": List[str], "default": ["java", "-jar"], "arg": None, "pos": 0},
    "self_set": {"type": str, "default": None, "arg": "-self"},
    "monitor_set": {"type": str, "default": None, "arg": "<", "pos": -2},
    "fname": {"type": str, "default": "temp", "arg": ">", "pos": -1},
    "alphabet": {"type": str, "default": None, "arg": "-alphabet"},
    "digit": {"type": int, "default": None, "arg": "-d"},
    "n": {"type": int, "default": None, "arg": "-n"},
    "component": {"type": int, "default": None, "arg": "-p"},
    "r": {"type": int, "default": None, "arg": "-r"},
    "count_detectors": {"type": bool, "default": True, "arg": "-c"},
    "debug": {"type": bool, "default": False, "arg": "-g"},
    "r_chunk": {"type": bool, "default": False, "arg": "-k"},
    "log": {"type": bool, "default": False, "arg": "-l"},
    "offset": {"type": bool, "default": False, "arg": "-o"},
    "invert_match": {"type": bool, "default": False, "arg": "-v"},
}

# get path to language files from the file name
get_lang_path = lambda x: full_path(BASE_DIR, os.getenv("LANG_PATH"), x)
# settings for pipeline of exercise 1
EXERCISE_1_SETUP = {
    # path to the self set
    "self_set": get_lang_path("english.train"),
    # language code of the self set
    "self_set_code": "en",
    # list of parameters r to consider
    "r": list(range(1, 10)),
    # length of strings
    "n": 10,
    # monitor sets as a dictionary of language code and language file path
    "monitor_sets": {
        "en": get_lang_path("english.test"),
        "tl": get_lang_path("tagalog.test"),
        "hil": get_lang_path("hiligaynon.txt"),
        "mid_en": get_lang_path("middle-english.txt"),
        "ptd": get_lang_path("plautdietsch.txt"),
        "xh": get_lang_path("xhosa.txt"),
    },
    # the path to the file storing NegSel2's results
    "raw_results_file": full_path(BASE_DIR, "data/ex_01_raw.csv"),
    # the path to the directory storing ROC figures
    "figure_path": full_path(BASE_DIR, "figures"),
    # the path to the R script for obtaining ROC figures
    "r_exec_path": full_path(BASE_DIR, "src/roc_curves.R"),
    # the path to the file to store results in a csv format.
    "csv_path": full_path(BASE_DIR, "data/ex_01_auc.csv"),
}

# labels, test files (monitor sets), train files (self sets),
# and alphabets of syscalls for the second exercise.
# Only needs to be edited if you change the filenames.
# obtain syscall_cert paths from the file name
get_syscall_cert_path = lambda x: full_path(BASE_DIR, os.getenv("SND_CERT_PATH"), x)
# obtain syscall unm paths from the file name
get_syscall_unm_path = lambda x: full_path(BASE_DIR, os.getenv("SND_UNM_PATH"), x)
SYSCALLS = {
    "snd_cert": {
        "labels": [
            get_syscall_cert_path("snd-cert.1.labels"),
            get_syscall_cert_path("snd-cert.2.labels"),
            get_syscall_cert_path("snd-cert.3.labels"),
        ],
        "tests": [
            get_syscall_cert_path("snd-cert.1.test"),
            get_syscall_cert_path("snd-cert.2.test"),
            get_syscall_cert_path("snd-cert.3.test"),
        ],
        "train": get_syscall_cert_path("snd-cert.train"),
        "alpha": get_syscall_cert_path("snd-cert.alpha"),
    },
    "snd_unm": {
        "labels": [
            get_syscall_unm_path("snd-unm.1.labels"),
            get_syscall_unm_path("snd-unm.2.labels"),
            get_syscall_unm_path("snd-unm.3.labels"),
        ],
        "tests": [
            get_syscall_unm_path("snd-unm.1.test"),
            get_syscall_unm_path("snd-unm.2.test"),
            get_syscall_unm_path("snd-unm.3.test"),
        ],
        "train": get_syscall_unm_path("snd-unm.train"),
        "alpha": get_syscall_unm_path("snd-unm.alpha"),
    },
}

EXERCISE_2_SETUP = {
    "n": 10,
    "rs": [3, 4],
    "source_files": SYSCALLS,
    "results_dir": full_path(BASE_DIR, "data/"),
    "figure_path": full_path(BASE_DIR, "figures"),
    "r_exec_path": full_path(BASE_DIR, "src/roc_curves.R"),
}
