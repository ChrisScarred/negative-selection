"""Implementation(s) of negative selection models.
"""

import logging
from typing import Any, List, Dict, Union, Tuple
from abc import ABC, abstractmethod
import os


class NegativeSelector(ABC):
    """Base abstract class for a negative selection model."""

    @abstractmethod
    def run(self, **kwargs):
        """Perform negative selection with the specified optional arguments."""
        pass

    @abstractmethod
    def set_arg(self, arg_name: str, arg_val: Any):
        """Set the value of an argument."""
        pass


class TextorNegativeSelector(NegativeSelector):
    """Python wrapper for Textor's NegSel2 negative selection algorithm. Based on NegativeSelection abstract class."""

    def __init__(
        self,
        jar_path: str,
        arg_dict: Dict[str, Dict[str, Union[List[str], str, int, bool, None]]],
    ) -> None:
        """Initialise Textor's negative selection.

        Args:
            jar_path (str): The path to NegSel2's jar.
            arg_dict (Dict[str, Dict[str, Union[List[str], str, int, bool, None]]]):
            The dictionary of NegSel2's CLI arguments as a map of natural language command name,
            and a dictionary of the command's type, default value, argument string as expected by NegSel2,
            and optionally position of the argument if it is fixed. Positions of 0 and more indicate
            the position as an index from the beginning of the argument list and negative position values
            indicate the x-th position from the end of the argument list, i.e., -1 indicates the last argument.
        """
        self.exec_path = jar_path
        self.arg_dict = arg_dict

    def set_arg(self, arg_name: str, arg_val: Any) -> None:
        """Set an argument's value.

        Args:
            arg_name (str): The argument name in natural language (not the string that NegSel2 expects).
            arg_val (Any): The value of the argument.
        """
        attr = self.arg_dict.get(arg_name)
        if isinstance(attr, dict):
            if isinstance(arg_val, attr.get("type")):
                self.arg_dict[arg_name]["value"] = arg_val
            else:
                logging.WARN(
                    f"Attempted to set arg {arg_name} to value {arg_val} of unexpected type ({type(arg_val)} while expecting type {attr.get('type')})"
                )
        elif arg_name == "self_set":
            if isinstance(arg_val, str):
                self.self_set = arg_val
            else:
                logging.WARN(
                    f"Attempted to set arg {arg_name} to value {arg_val} of unexpected type ({type(arg_val)} while expecting type str)"
                )
        elif arg_name == "monitor_set":
            if isinstance(arg_val, str):
                self.monitor_set = arg_val
            else:
                logging.WARN(
                    f"Attempted to set arg {arg_name} to value {arg_val} of unexpected type ({type(arg_val)} while expecting type str)"
                )
        else:
            logging.WARN(f"Attempted to set non-existent arg {arg_name}")

    def _parse_args_kwargs(self, kwargs: Dict[str, Any]) -> Tuple[str, str]:
        """Parse the currently set args and provided kwargs into a command. Kwargs take priority over args,
        arg values take priority over arg defaults.

        Args:
            kwargs (Dict[str, Any]): The provided kwargs.

        Returns:
            Tuple[str, str]: The full command as a string followed by the path to the temporary file storing NegSel2's output.
        """
        # hold the command, i.e., the 0-th arg
        command = []
        # hold commands whose position is not set
        mid_args = []
        # hold commands that have a required position
        positioned_args = {}
        # hold filename
        fname = ""
        # hold positions of positioned args
        positions = []

        for attr_name, attr_dict in self.arg_dict.items():
            # first attempt to get the value from kwargs, then from custom-set value in self.arg_dict
            # and only then use the default
            attr_val = kwargs.get(
                attr_name, attr_dict.get("value", attr_dict.get("default"))
            )
            # check if the value is not False (when False, a flag is not added)
            # or None (when None, the arg is not supplied)
            if attr_val:
                p = attr_dict.get("pos")
                if isinstance(p, int):
                    if p == 0:
                        command.extend(
                            [
                                " ".join(attr_val),
                                kwargs.get("exec_path", self.exec_path),
                            ]
                        )
                    else:
                        positioned_args[p] = [
                            attr_dict.get("arg"),
                            attr_val,
                        ]
                        positions.append(p)
                        if attr_name == "fname":
                            fname = attr_val
                else:
                    mid_args.append(attr_dict.get("arg"))
                    # if bool, only 'arg' aka the flag is added to the command
                    if not isinstance(attr_val, bool):
                        mid_args.append(str(attr_val))

        for i in sorted([i for i in positions if i > 0]):
            command.extend(positioned_args[i])
        command.extend(mid_args)
        for i in sorted([i for i in positions if i < 0]):
            if i in positions:
                command.extend(positioned_args[i])
        return " ".join(command), fname

    def run(self, **kwargs) -> str:
        """Run the negative selection. Any argument's value can be set as a kwarg,
        in which case it takes precedence over it's set or default value. If the value
        of an argument was set for this instance and no kwarg overrides it, the set value is used,
        otherwise the default value is used.

        Returns:
            str: The path to the temporary file that stores the results of this run.
        """
        command, fname = self._parse_args_kwargs(kwargs)
        os.system(command)
        return fname


def type_check(value: Any, type: Any, default: Any = None) -> Any:
    """Check that a value is of a required type and if so, return the value. Return the default otherwise."""
    if isinstance(value, type):
        return value
    return default
