"""Perform negative selection and obtain relevant statistics.
"""

import numpy as np
import pandas as pd
from negative_selection import NegativeSelector, TextorNegativeSelector
from typing import Dict, List
import os
from preprocess import preprocess_syscalls
from classifier import NegativeSelectionClassifier


def get_selector(
    jar_path: str, selector: NegativeSelector, arg_dict: Dict, preset_args: Dict
) -> NegativeSelector:
    """Initialise a selector object such that the function run() can be called on it.

    Args:
        jar_path (str): The path to selector's jar file.
        selector (NegativeSelector): The type of the selector.
        The only currently existing type is TextorNegativeSelector.
        arg_dict (Dict): The dictionary of arguments this selector expects.
        Further documentation can be seen in negative_selection.py's TextorNegativeSelection docstrings.
        preset_args (Dict): The set of arguments with which to initialise this selector.

    Returns:
        NegativeSelector: An initialised selector object.
    """
    s = selector(jar_path, arg_dict)
    for arg_name, arg_val in preset_args.items():
        s.set_arg(arg_name, arg_val)
    return s


def multi_r_negative_selection(
    jar_path: str,
    selector: NegativeSelector,
    rs: List[int],
    monitor_sets: List[str],
    arg_dict: Dict,
    preset_args: Dict,
    self_set_code: str,
    raw_results_file: str,
    fig_path: str,
    roc_exec_path: str,
    csv_path: str,
):
    """Perform negative selection on (optionally multiple) monitor sets with (optionally multiple) different parameter values for r
    (such that for each r a new model is built).

    Args:
        jar_path (str): The path to selector's jar file.
        selector (NegativeSelector): The type of selector to use. The only currently existing type is TextorNegativeSelector.
        rs (List[int]): The list of r parameter values with which to run negative selection models.
        monitor_sets (List[str]): The list of monitor sets as file paths on which to test the negative selection model(s).
        arg_dict (Dict): The dictionary of arguments this selector expects.
        Further documentation can be seen in negative_selection.py's TextorNegativeSelection docstrings.
        preset_args (Dict): The set of arguments which are fixed for each considered negative selection model.
        self_set_code (str): The language code of the self set.
        raw_results_file (str): The path to the file to store anomaly scores of each word of each monitor set under every model.
        Expected .csv extension.
        fig_path (str): The path to the folder to store ROC figures.
        roc_exec_path (str): The path to the R script that creates ROC figures from raw data.
        csv_path (str): The path to the file to store results in a csv format.
    """
    get_raw_results(
        raw_results_file, jar_path, selector, rs, monitor_sets, arg_dict, preset_args
    )
    get_auc_roc(raw_results_file, fig_path, self_set_code, roc_exec_path, csv_path)


def get_auc_roc(
    raw_results_file: str,
    fig_path: str,
    self_set_code: str,
    exec_path: str,
    csv_path: str,
):
    """Create ROC figures from raw data utilising an R script.

    Args:
        raw_results_file (str): The path to the file storing raw results.
        fig_path (str): The path to the directory storing ROC figure.
        self_set_code (str): The language code of the self set.
        exec_path (str): The path to the R script that creates the ROC figures.
        csv_path (str): The path to the file to store results in a csv format.
    """
    os.system(
        f"Rscript --vanilla {exec_path} {raw_results_file} {self_set_code} {fig_path} {csv_path}"
    )


def run_negative_selection(
    raw_results_file: str,
    jar_path: str,
    selector: NegativeSelector,
    rs: List[int],
    monitor_sets: List[str],
    arg_dict: Dict,
    preset_args: Dict,
) -> pd.DataFrame:
    """Perform negative selection with multiple models for different values of r with multiple monitor sets.

    Args:
        raw_results_file (str): The path to the file to store the raw results.
        jar_path (str): The path to the jar file of the selector.
        selector (NegativeSelector): The type of the selector.
        rs (List[int]): The list of values of r to use.
        monitor_sets (List[str]): The list of monitor sets as paths to the language files.
        arg_dict (Dict): The dictionary of arguments the selector expects.
        Further documentation can be seen in negative_selection.py's TextorNegativeSelection docstrings.
        preset_args (Dict): The dictionary of arguments that are fixed for each model.

    Returns:
        pd.DataFrame: The raw results as a dataframe of the anomaly score of each word
        of a given language under a model with a given r.
    """
    selector = get_selector(jar_path, selector, arg_dict, preset_args)
    full_results = []
    for r in rs:
        for code, monitor_set in monitor_sets.items():
            fname = selector.run(monitor_set=monitor_set, r=r)
            results = read_file(fname)
            os.remove(fname)
            full_results.extend(
                [
                    {"Language": code, "r": r, "Word": i, "Value": v}
                    for i, v in enumerate(results)
                ]
            )

    df = pd.DataFrame(data=full_results)
    df.to_csv(raw_results_file)
    return df


def get_raw_results(
    raw_results_file: str,
    jar_path: str,
    selector: NegativeSelector,
    rs: List[int],
    monitor_sets: List[str],
    arg_dict: Dict,
    preset_args: Dict,
):
    """Obtain the raw results by reading the raw results file if it exists
    and otherwise by creating it via running the negative selection algorithm.
    """
    if os.path.isfile(raw_results_file):
        return pd.read_csv(raw_results_file)
    return run_negative_selection(
        raw_results_file, jar_path, selector, rs, monitor_sets, arg_dict, preset_args
    )


def read_file(fname: str) -> np.ndarray:
    """Read a file of strings of floats. Necessary to obtain the data
    from NegSel2 because it is currently not possible to fetch them directly from stdout.
    """
    results = []
    with open(fname, "r") as f:
        results = [float(line) for line in f.readlines()]
    return np.asarray(results)


def mean_str_len(syscall_dict: Dict):
    files = []
    for type_dict in syscall_dict.values():
        files.append(type_dict.get("train"))
        files.extend(type_dict.get("tests"))
    lens = []
    for file in files:
        with open(file, "r") as f:
            lens.extend([len(line.strip("\n")) for line in f.readlines()])
    lens = np.array(lens)
    print(
        f"Average string length: {np.mean(lens)}\nMedian string length: {np.median(lens)}"
    )


def classifier_multi_r(
    n: int,
    preset_args: Dict,
    selector: NegativeSelector,
    rs: List[int],
    jar_path: str,
    negsel2_args: Dict,
    source_files: Dict,
    results_dir: str,
    fig_path: str,
    roc_exec_path: str,
):
    selector = get_selector(jar_path, selector, negsel2_args, preset_args)
    preprocessor = lambda x: preprocess_syscalls(x, n)
    classifier = NegativeSelectionClassifier(selector, preprocessor)
    classifier.load_data(source_files)

    for r in rs:
        res = classifier.classify(r)
        for set_, set_dict in res.items():
            self_set = set_dict.get("self_set")
            res = pd.DataFrame(data=set_dict.get("results"))
            raw_fn = f"{results_dir}ex_02_raw_{set_}.csv"
            res.to_csv(raw_fn)
            auc_fn = f"{results_dir}ex_02_auc_{set_}.csv"
            get_auc_roc(raw_fn, fig_path, self_set, roc_exec_path, auc_fn)
