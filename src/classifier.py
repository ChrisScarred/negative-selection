from negative_selection import NegativeSelector
from typing import Callable, Dict, List
import numpy as np
import pandas as pd


class NegativeSelectionClassifier:
    def __init__(self, selector: NegativeSelector, preprocessor: Callable) -> None:
        self.selector = selector
        self.preprocessor = preprocessor
        self.data = {}

    def load_data(self, paths: Dict) -> None:
        self.data = self.preprocessor(paths)

    def _run(
        self,
        self_set: str,
        alphabet: str,
        monitor_set: str,
        labels: str,
        chunkmap: str,
        r: int,
    ) -> pd.DataFrame:
        first_part, set_no, _, _ = monitor_set.split(".")
        lang_name = first_part.split("/")[-1]
        dir_ = first_part.split(lang_name)[0]
        fname = self.selector.run(
            self_set=self_set,
            monitor_set=monitor_set,
            alphabet=alphabet,
            r=r,
            fname=f"{dir_}{lang_name}/{lang_name}_{set_no}.raw_results",
        )
        with open(fname, "r") as f:
            raw_results = np.asarray([float(line) for line in f.readlines()])
        results = []
        with open(labels, "r") as l:
            label_list = [int(line.strip("\n")) for line in l.readlines()]
        with open(chunkmap, "r") as chunks:
            for line in chunks.readlines():
                word, start, end = (int(x) for x in line.strip("\n").split(", "))
                results.append(
                    {
                        "Language": lang_name+set_no,
                        "r": r,
                        "Word": word,
                        "Label": label_list[word],
                        "Value": np.mean(np.array(raw_results[start:end])),
                    }
                )
        return pd.DataFrame(data=results)

    def classify(self, r: int) -> List[pd.DataFrame]:
        dfs = {}
        for type_name, type_dict in self.data.items():
            alphabet = type_dict.get("alpha")
            self_set = type_dict.get("train")
            test_sets = type_dict.get("tests")
            train_chunkmap = type_dict.get("train_chunkmap")
            with open(train_chunkmap, "r") as f:
                results = pd.DataFrame(
                    data=[
                        {
                            "Language": f"{type_name}_self",
                            "r": r,
                            "Word": i,
                            "Label": 0,
                            "Value": 0,
                        }
                        for i in range(len(f.readlines()))
                    ]
                )
            for test_set in test_sets:
                monitor_set = test_set.get("data")
                set_no = monitor_set.split(".")[1]
                labels = test_set.get("labels")
                chunkmap = test_set.get("chunkmap")
                res = self._run(self_set, alphabet, monitor_set, labels, chunkmap, r)
                results = pd.concat([results, res], ignore_index=True)
                dfs[f"{type_name}{set_no}"] = {
                    "results": results,
                    "self_set": f"{type_name}_self",
                }
        return dfs
