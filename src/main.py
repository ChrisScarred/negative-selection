"""Pipelines for the exercises.
"""

from stats import multi_r_negative_selection, classifier_multi_r
from config import JAR_PATH, EXERCISE_1_SETUP, NEGSEL2_ARGS, EXERCISE_2_SETUP
from negative_selection import TextorNegativeSelector


def exercise_1(log: bool = True):
    """Perform all steps required to answer the questions of exercise 1.

    Args:
        log (bool, optional): If True, obtain anomaly scores as logs. Defaults to True.
    """
    preset_args = {
        "self_set": EXERCISE_1_SETUP.get("self_set"),
        "n": EXERCISE_1_SETUP.get("n"),
        "log": log,
    }
    multi_r_negative_selection(
        JAR_PATH,
        TextorNegativeSelector,
        EXERCISE_1_SETUP.get("r"),
        EXERCISE_1_SETUP.get("monitor_sets"),
        NEGSEL2_ARGS,
        preset_args,
        EXERCISE_1_SETUP.get("self_set_code"),
        EXERCISE_1_SETUP.get("raw_results_file"),
        EXERCISE_1_SETUP.get("figure_path"),
        EXERCISE_1_SETUP.get("r_exec_path"),
        EXERCISE_1_SETUP.get("csv_path"),
    )


def exercise_2(log: bool = True):
    """Perform all steps required to answer the questions of exercise 2.

    Args:
        log (bool, optional): If True, obtain anomaly scores as logs. Defaults to True.
    """
    n = EXERCISE_2_SETUP.get("n")
    preset_args = {"n": n, "log": log}

    classifier_multi_r(
        n,
        preset_args,
        TextorNegativeSelector,
        EXERCISE_2_SETUP.get("rs"),
        JAR_PATH,
        NEGSEL2_ARGS,
        EXERCISE_2_SETUP.get("source_files"),
        EXERCISE_2_SETUP.get("results_dir"),
        EXERCISE_2_SETUP.get("figure_path"),
        EXERCISE_2_SETUP.get("r_exec_path"),
    )


if __name__ == "__main__":
    exercise_1()
    exercise_2()
