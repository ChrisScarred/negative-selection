# Natural Computing Assignment 3: Negative Selection

## Instructions

1. Download Textor's negative selection algorithm `negsel2.jar` and all required data.
2. Place `negsel2.jar` into the folder `vendor` (or your own preferred location).
3. Place all language data into the same folder, for example `data/lang`.
4. Install the Python dependencies - versions indicated in `pyproject.toml` certainly work, but other versions likely work as well.
5. Copy `.env.example` to `.env` and adjust paths as needed. `SND_CERT_PATH` and `SND_UNM_PATH` are not yet used.
6. Run `src/main.py` with Python from the root folder of this project.


## Negsel2 CLI

`negsel2.jar` has the following options and flags:

- `-alphabet <arg>` where `<arg>` is one of `[infer|binary|binaryletter|amino|damino|latin|file://[f]]`. Defaults to `infer` which uses all characters from the self file. `file://[f]` uses all characters in file `[f]`.
- `-c` to count matching detectors instead of performing binary match which outputs the length of the longest string that is shared with the input.
- `-d <arg>` to expand the alphabet with the digits from 0 to `<arg>`.
- `-g` to print debug info.
- `-k` to use r-chunk instead of r-contiguous matching.
- `-l` to output logarithms instead of actual values.
- `-n <arg>` to set the length of strings in the self set.
- `-o` to offset into strings.
- `-p <arg>` to output k-th component of the matching profile (0 for full profile).
- `-r <arg>` to set parameter r to `<arg>` such that `r <= n`.
- `-self <arg>` to set the path to the file containing self set (1 string per line).
- `-v` to perform invert match (like grep).
